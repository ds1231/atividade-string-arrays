/*
Crie um programa que peça ao usuario para inserir os valores de uma matriz 3x4. Mostre a somatoria dos números inseridos
e depois printe em tela a matriz.
Por fim, imprima no console o valor do número da posição [1][2].
Entrega até 20/05 no classroom.
*/
#include <stdio.h>
#define LINHAS 3
#define COLUNAS 4

int i, i2, num, soma, matriz [LINHAS] [COLUNAS];

int main () {

    for (i = 0; i <LINHAS; i++) {
        for (i2=0; i2 < COLUNAS; i2++) {
            printf ("Elemento da Linha %d, Coluna %d: ", i, i2);
            scanf ("%d", &num);
            matriz [i] [i2] = num;
        }
    }

    for (i = 0; i <LINHAS; i++) {
        for (i2=0; i2 < COLUNAS; i2++) {
            soma = soma + matriz [i] [i2];          
        }
    }

printf ("Somatoria dos elementos: %d\n", soma);

printf ("Matriz:\n");
    for (i = 0; i <LINHAS; i++) {
        printf ("| ");
        for (i2=0; i2 < COLUNAS; i2++) {
            num = matriz [i][i2];
            printf ("%d ", num);
        }
    printf (" |\n");
    }

printf ("Número na posição [1] [2]: %d", matriz[1][2]);

return 0;
}
